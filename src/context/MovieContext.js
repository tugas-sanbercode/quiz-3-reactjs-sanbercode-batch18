import React, {useState, createContext} from "react";

export const MovieContext = createContext();

export const MovieProvider = props => {
    
    const [Movie, setMovie] = useState(null)

    return (
        <MovieContext.Provider value={[Movie, setMovie]}>
            {props.children}
        </MovieContext.Provider>
    )
}