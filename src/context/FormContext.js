import React, {useState, createContext} from "react";

export const FormContext = createContext();

export const FormProvider = props => {
    
    const [input, setInput] = useState({
        "id": '',
        "created_at": '',
        "updated_at": '',
        "title": '',
        "description": '',
        "year": 2020,
        "duration": 120,
        "genre": '',
        "rating": 0,
        "review": '',
        "image_url": '',
    })

    return (
        <FormContext.Provider value={[input, setInput]}>
            {props.children}
        </FormContext.Provider>
    )
}