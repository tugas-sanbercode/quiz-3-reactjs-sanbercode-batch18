import React, {useState, createContext, useEffect, useContext} from "react";
import { UserContext } from "./UserContext";

export const LoginContext = createContext();

export const LoginProvider = props => {
    
    const [user] = useContext(UserContext)
    const [isLogin, setIsLogin] = useState(0)

    useEffect( () =>{

        let username =  window.localStorage.getItem('username');
        let pass = window.localStorage.getItem('pass');

        if(user.username === username && user.pass === pass && !isLogin){
            setIsLogin(1)
        }

        console.log('isLogin: ' + isLogin)
    })

    return (
        <LoginContext.Provider value={[isLogin, setIsLogin]}>
            {props.children}
        </LoginContext.Provider>
    )
}