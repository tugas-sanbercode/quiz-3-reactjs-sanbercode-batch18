import React from 'react';
import logo from './assets/logo.png';
import './assets/style.css';
import {UserProvider} from "./context/UserContext"
import {LoginProvider} from "./context/LoginContext"
import {BrowserRouter as Router} from "react-router-dom";
import Nav from './components/Nav.js';
import Content from './components/Section.js';

function App() {
  return (
    
    <Router> 
      <header>
        <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet" />
        <img id="logo" src={logo} width="200px" alt="Logo" />
        <UserProvider>
          <LoginProvider>
            <Nav />
          </LoginProvider>
        </UserProvider>
      </header>
      <section>
        <UserProvider>
          <LoginProvider>
            <Content/>
          </LoginProvider>
        </UserProvider>
      </section>
      <footer>
        <h5>copyright &copy; 2020 by Sanbercode</h5>
      </footer>
    </Router> 
  );
}

export default App;
