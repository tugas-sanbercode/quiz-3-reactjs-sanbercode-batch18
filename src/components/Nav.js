import React, {useContext} from "react";
import {Link} from "react-router-dom";
import {LoginContext} from "../context/LoginContext";

const Nav = () => {
    const [isLogin] = useContext(LoginContext)

    return (
        <>
            <nav>
                <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/about">About</Link>
                </li>
                { isLogin ?
                <li>
                    <Link to="/editor">Movie List Editor</Link>
                </li>
                : <></>
                }
                <li>
                    { isLogin ? 
                    <Link to="/logout">Logout</Link> 
                    : 
                    <Link to="/login">Login</Link>}           
                </li>
                </ul>
            </nav>
        </>
    )
}

export default Nav