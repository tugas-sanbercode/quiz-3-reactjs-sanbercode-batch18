import React, {useContext} from 'react';
import {MovieContext} from "../context/MovieContext";
import {FormContext} from '../context/FormContext';
import axios from 'axios';

const MovieForm = () =>{

    const [, setMovie] = useContext(MovieContext);
    const [input, setInput] = useContext(FormContext);

    const addMovie = (Movie) =>{
        var queryString = Object.keys(Movie).map(key => key + '=' + Movie[key]).join('&');
        axios.post(`http://backendexample.sanbercloud.com/api/movies`, queryString)
        .then(res => {
            console.log(res)
            setMovie(null)
        })
    }

    const editMovie = (id, Movie) =>{

        var queryString = Object.keys(Movie).map(key => key + '=' + Movie[key]).join('&');

        axios.put(`http://backendexample.sanbercloud.com/api/movies/${id}`, queryString)
        .then(res => {
            console.log(res)
            setMovie(null)
        })

        console.log('edit Movie')
    }
    const HandleChangeInput = (event) =>{

        let type = event.target.name
        let value = event.target.value

        setInput({...input, [type]:value})
    }

    const HandleSubmit = (event) =>{

        let id = input.id
        let title = input.title;
        let description = input.description;
        let year = input.year;
        let duration = input.duration;
        let genre = input.genre;
        let rating = input.rating;
        let image_url = input.image_url;

        //handle add list
        if(!id){
            addMovie({
                title,
                description,
                year,
                duration,
                genre,
                rating,
                image_url,
            })
            
            ResetForm()
        }
        // handle edit list
        else{
            editMovie(id,{
                title,
                description,
                year,
                duration,
                genre,
                rating,
                image_url,
            })

            ResetForm()
        }
        
        event.preventDefault()
    }

    const ResetForm = () =>{
        setInput({
            "id": '',
            "created_at": '',
            "updated_at": '',
            "title": '',
            "description": '',
            "year": 2020,
            "duration": 120,
            "genre": '',
            "rating": 0,
            "review": '',
            "image_url": '',
        })

        setMovie(null)
    }

    return (
        <>
            <div id="form">
                <form method="post" onSubmit={HandleSubmit}>
                <h2>Movie Form</h2>
                <div className="form-group">
                    <label htmlFor="#title">Title</label>
                    <div className="input-wrap">
                    <input type="text" id="title" name="title" value={input.title} onChange={HandleChangeInput} required />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="#description">Description</label>
                    <div className="input-wrap">
                    <textarea id="description" name="description" value={input.description} onChange={HandleChangeInput}  />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="#year">Year</label>
                    <div className="input-wrap">
                    <input type="number" id="year" name="year" value={input.year} onChange={HandleChangeInput} required />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="#duration">Duration (minutes)</label>
                    <div className="input-wrap">
                    <input type="number" id="duration" name="duration" value={input.duration} onChange={HandleChangeInput} required />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="#genre">Genre</label>
                    <div className="input-wrap">
                    <input type="text" id="genre" name="genre" value={input.genre} onChange={HandleChangeInput} required />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="#rating">Rating</label>
                    <div className="input-wrap">
                    <input type="number" id="rating" name="rating" value={input.rating} onChange={HandleChangeInput} required />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="#image_url">Image Url</label>
                    <div className="input-wrap">
                    <textarea id="image_url" name="image_url" value={input.image_url} onChange={HandleChangeInput}  />
                    </div>
                </div>                
                <div className="form-group">
                    <div className="input-wrap">
                    <input type="submit" value="Kirim" />
                    </div>
                </div>
                </form>
            </div>
        </>
    )
}

export default MovieForm