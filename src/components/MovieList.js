import React, {useContext, useEffect} from "react"
import {MovieContext} from "../context/MovieContext"
import {FormContext} from "../context/FormContext"
import axios from 'axios';

const MovieList = () =>{

    const [Movie, setMovie] = useContext(MovieContext)
    const [, setInput] = useContext(FormContext)

    useEffect( () => {

        getDaftarMovie()
    })

    const getDaftarMovie = () => {

        if(Movie===null){
            axios.get('http://backendexample.sanbercloud.com/api/movies')
            .then(res => {
                setMovie(res.data.map( (el) =>{
                    return {
                        "id": el.id,
                        "created_at": el.created_at,
                        "updated_at": el.updated_at,
                        "title": el.title,
                        "description": el.description,
                        "year": el.year,
                        "duration": el.duration,
                        "genre": el.genre,
                        "rating": el.rating,
                        "review": el.review,
                        "image_url": el.image_url,
                    }
                }))
            })
            console.log('get Movie')
      }
    }

    const HandleRemove = (event) =>{
        
        let id = event.target.value

        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${id}`)
        .then(res => {
            setMovie(null)
            console.log(res)
        })
    }    

    const HandleEdit = (event) =>{
        
        let id = event.target.value

        axios.get(`http://backendexample.sanbercloud.com/api/movies/${id}`)
        .then(res => {
            let el = res.data
            setInput({
                    "id": el.id,
                    "created_at": el.created_at,
                    "updated_at": el.updated_at,
                    "title": el.title,
                    "description": el.description,
                    "year": el.year,
                    "duration": el.duration,
                    "genre": el.genre,
                    "rating": el.rating,
                    "review": el.review,
                    "image_url": el.image_url,
                })
        })
    }

    return(
        <>
            <h2>Movie List</h2>
            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Year</th>
                        <th>Duration</th>
                        <th>Genre</th>
                        <th>Rating</th>
                        <th>Action</th>
                    </tr>    
                </thead>
                <tbody>
                { Movie!==null && Movie.map( (el, index) =>{
                        return  <tr key={index}>
                                  <td>{index+1}</td>
                                <td>{el.title}</td>
                                <td>{el.description}</td>
                                <td>{el.year}</td>
                                <td>{el.duration}</td>
                                <td>{el.genre}</td>
                                <td>{el.rating}</td>
                                <td><button value={el.id} onClick={HandleEdit}>Edit</button> | <button value={el.id} onClick={HandleRemove}>Hapus</button></td>
                            </tr>                        
                        })
                }
                
                </tbody>
            </table>
        </>
    )
}

export default MovieList