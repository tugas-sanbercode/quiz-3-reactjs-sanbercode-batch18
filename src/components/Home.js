import React from "react"
import HomeList from './HomeList'
import {MovieProvider} from '../context/MovieContext'

const Home = () =>{

    return (
        <MovieProvider>
            <h2>Best Movie List</h2>
            <HomeList/>
        </MovieProvider>
    )
}

export default Home