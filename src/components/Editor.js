import React, { useContext, useEffect } from "react"
import {MovieProvider} from "../context/MovieContext"
import {FormProvider} from "../context/FormContext"
import MovieList from "./MovieList"
import MovieForm from "./MovieForm"
import { LoginContext } from "../context/LoginContext"
import { UserContext } from "../context/UserContext"

const Editor = () =>{

    const [user] = useContext(UserContext)
    const [isLogin, setIsLogin] = useContext(LoginContext)

    useEffect( ()=>{
        
        let username = window.localStorage.getItem('username');
        let pass = window.localStorage.getItem('pass');
        
        if(user.username===username && user.pass===pass){
            setIsLogin(1)
        }else{
            window.location.href = '/login';
        }
    })

    return(
        <MovieProvider>
            <FormProvider>
                {isLogin ?
                <>
                    <MovieList/>
                    <MovieForm/>
                </>
                : <>redirecting...</> }
            </FormProvider>
        </MovieProvider>
    )
}

export default Editor