import React from "react";
import { Switch, Route } from "react-router";
import Home from "./Home.js"
import About from "./About.js"
import Editor from "./Editor.js"
import Login from "./Login.js"
import Logout from "./Logout.js"

const Section = () => {

  return (
    <>
      <Switch>
        <Route exact path="/">
            <Home/>
        </Route>
        <Route path="/about">
            <About/>
        </Route>
        <Route path="/editor">
            <Editor/> 
        </Route>
        <Route path="/login">
            <Login/>
        </Route>
        <Route path="/logout">
            <Logout/>
        </Route>
      </Switch>
    </>
  );
};

export default Section;