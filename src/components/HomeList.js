import React, { useContext, useEffect } from "react"
import {MovieContext} from '../context/MovieContext'
import axios from "axios"

const HomeList = () =>{

    const [Movie, setMovie] = useContext(MovieContext)

    useEffect( ()=>{
        getDaftarMovie()
    })

    const getDaftarMovie = () => {

        if(Movie===null){
            axios.get('http://backendexample.sanbercloud.com/api/movies')
            .then(res => {
                setMovie(res.data.map( (el) =>{
                    return {
                        "id": el.id,
                        "created_at": el.created_at,
                        "updated_at": el.updated_at,
                        "title": el.title,
                        "description": el.description,
                        "year": el.year,
                        "duration": el.duration,
                        "genre": el.genre,
                        "rating": el.rating,
                        "review": el.review,
                        "image_url": el.image_url,
                    }
                }))
            })
            console.log('get Movie')
      }
    }

    return (
        <div id="article-list">
        {
            Movie !== null && Movie.map( (el, index) =>{
                return (
                    <div key={index}>
                        <h3>{el.title}</h3>
                        <div className="img-wrap">
                            <div className="img">
                                <img src={el.image_url} alt="el.title" />
                            </div>
                            <div className="att">
                                <ul>
                                     <li>Rating {el.rating}</li>
                                     <li>Duration: {el.duration} minutes</li>
                                     <li>Genre: {el.genre}</li>
                                </ul>
                            </div>
                            <div className="clearfix"></div>
                        </div>
                        <p>{el.description}</p>
                    </div>
                )
            })
        }
      </div>
    )
}

export default HomeList