import React from 'react';

const About = () =>{

    return (
        <div className="wrap border">
		  <h1>Data Peserta Sanbercode Bootcamp Reactjs</h1>
		  <div id="profile">
			<ol>
				<li><strong>Nama:</strong> Deczen De Kristo</li>
				<li><strong>Email:</strong> deczen.work@gmail.com</li>
				<li><strong>Sistem Operasi yang digunakan:</strong> Windows 10</li>
				<li><strong>Akun Gitlab:</strong> @deczen</li>
				<li><strong>Akun Telegram:</strong> @deczen</li>			
			</ol>
		  </div>
		</div>
    )
}

export default About