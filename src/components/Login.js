import React, {useContext, useState, useEffect} from "react"
import {UserContext} from "../context/UserContext"
import {LoginContext} from "../context/LoginContext"

const Login = () =>{

    const [user] = useContext(UserContext)
    const [, setIsLogin] = useContext(LoginContext)
    const [input, setInput] = useState({
        username: '',
        pass: '',
    })

    useEffect( ()=>{
        let username = window.localStorage.getItem('username');
        let pass = window.localStorage.getItem('pass');
        
        if(user.username===username && user.pass===pass){
            setIsLogin(1)
            window.location.href = '/editor';
        }
    })
    
    const HandleChangeInput = (event) =>{
        let type = event.target.name
        let value = event.target.value

        setInput({...input, [type]:value})
    }

    const HandleSubmit = (event) =>{
        
        let username = input.username
        let pass = input.pass;
        
        if(user.username===username && user.pass===pass){

            window.localStorage.setItem('username', username);
            window.localStorage.setItem('pass', pass);

            window.location.href = '/editor';
        }else{
            window.location.href = '/login';
        }
        event.preventDefault()
    }

    return (
        <>        
            <h2>Login</h2>
            <form onSubmit={HandleSubmit}>
                <div className="form-group">
                    <label htmlFor="#username">Username</label>
                    <div className="input-wrap">
                    <input type="text" id="username" name="username" value={input.username} onChange={HandleChangeInput} required />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="#pass">Password</label>
                    <div className="input-wrap">
                    <input type="password" id="pass" name="pass" value={input.pass} onChange={HandleChangeInput} required />
                    </div>
                </div>
                <div className="form-group">
                    <div className="input-wrap">
                    <input type="submit" value="Login" />
                    </div>
                </div>
            </form>
        </>
    )
}

export default Login